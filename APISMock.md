# Documentación de la API - Ruta Viva Mock

## Información General
mockId: "64dc13c7e64a8525a0f636a2"
prefix: "/ruta-viva"

### Recursos
La API ofrece el siguiente recurso:

#### Productos

ID: "64dc1493e64a8525a0f636db"
mockDataId: "64dc1493e64a8525a0f636da"
Nombre: "products"

##### Esquema:
```js
{
  id: Identificador único del producto (Tipo: Object ID)
  title: Título del producto (Tipo: String)
  description: Descripción del producto (Tipo: String)
  price: Precio del producto (Tipo: Number)
  discountPercentage: Porcentaje de descuento del producto (Tipo: Number)
  rating: Calificación del producto (Tipo: Number)
  stock: Cantidad en stock del producto (Tipo: Number)
  brand: Marca del producto (Tipo: String)
  category: Categoría del producto (Tipo: String)
  thumbnail: Miniatura del producto (Tipo: Object)
  images: Imágenes del producto (Tipo: Array)
}
```
#### Endpoints
La API ofrece los siguientes endpoints habilitados para interactuar con el recurso de productos:

#### Obtener todos los productos

URL: /products
Método: GET
Respuesta: Datos de productos (Ver esquema de productos)

#### Obtener un producto por ID

URL: /products/:id
Método: GET
Respuesta: Datos de un producto específico (Ver esquema de productos)

#### Crear un nuevo producto

URL: /products
Método: POST
Respuesta: Datos del nuevo producto creado (Ver esquema de productos)

#### Actualizar un producto por ID

URL: /products/:id
Método: PUT
Respuesta: Datos del producto actualizado (Ver esquema de productos)

#### Eliminar un producto por ID

URL: /products/:id
Método: DELETE
Respuesta: Datos del producto eliminado (Ver esquema de productos)
Respuestas
 
 
 ---

# Documentación de la API - Mock de usuarios

Información General
ID del mock: "64e36773bac46e480e78bfd7"
Prefijo: "/login"
Recursos
La API ofrece el siguiente recurso:

Usuarios
ID: "64e369d5bac46e480e78c411"
ID del mockData: "64e369d5bac46e480e78c410"

#### Nombre: "users"
Esquema:
```js
{
  id: Identificador único del usuario (Tipo: Object ID)
  firstName: Nombre del usuario (Tipo: String)
  lastName: Apellido del usuario (Tipo:String)
  maidenName: Apellido de soltera del usuario (Tipo: String)
  email: Correo electrónico del usuario (Tipo: String)
  phone: Número de teléfono del usuario (Tipo: String)
  username: Nombre de usuario del usuario (Tipo: String)
  password: Contraseña del usuario (Tipo: String)
  birthDate: Fecha de nacimiento del usuario (Tipo: Date)
  image: Imagen de perfil del usuario (Tipo: Object)
  role: Rol del usuario (Tipo: String)
}
```
Endpoints
La API ofrece los siguientes endpoints habilitados para interactuar con el recurso de usuarios:

Obtener todos los usuarios

URL: /users
Método: GET
Respuesta: Datos de usuarios (Ver esquema de usuarios)
Obtener un usuario por ID

URL: /users/:id
Método: GET
Respuesta: Datos de un usuario específico (Ver esquema de usuarios)
Crear un nuevo usuario

URL: /users
Método: POST
Respuesta: Datos del nuevo usuario creado (Ver esquema de usuarios)
Actualizar un usuario por ID

URL: /users/:id
Método: PUT
Respuesta: Datos del usuario actualizado (Ver esquema de usuarios)
Eliminar un usuario por ID

URL: /users/:id
Método: DELETE
Respuesta: Datos del usuario eliminado (Ver esquema de usuarios)
