import axios from 'axios';

const apiCategory = axios.create({
  baseURL: import.meta.env.VITE_CATEGORY_URL,
  timeout: 5000,
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  },
});


export default apiCategory;