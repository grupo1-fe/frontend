import axios from 'axios';


const apiCharacteristics = axios.create({
  baseURL: import.meta.env.VITE_CHARACTERISTICS_URL,
  timeout: 5000,
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  },
});


export default apiCharacteristics;