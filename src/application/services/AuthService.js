import axios from 'axios';

const loginService = axios.create({
  baseURL: import.meta.env.VITE_USER_URL,
  timeout: 5000,
});

export const loginUser = async (user, password) => {
  try {
    const response = await loginService.get(`?search=${user}&password=${password}`);
    
    if (response.data.length > 0) {
      const matchedUser = response.data.find(u => u.email === user && u.password === password);

      if (matchedUser) {
        return { success: true, data: matchedUser };
      }
    }
    
    return { success: false, message: 'Correo o contraseña incorrectos.' };

  } catch (error) {
    console.error('Error en el inicio de sesión:', error);
    return { success: false, message: 'Error en el inicio de sesión. Verifica tus credenciales.' };
  }
};


export const registerUser = async (userDetails) => {
  try {
    const response = await loginService.post('login', userDetails);
    return { success: true, data: response.data };
  } catch (error) {
    console.error('Error en el registro:', error);
    return { success: false, message: 'Error en el registro. Verifica tus datos.' };
  }
};


export const getUsers = async () => {
  try {
    const response = await loginService.get();
    return { success: true, data: response.data };
  } catch (error) {
    console.error('Error al obtener usuarios:', error);
    return { success: false, message: 'Error al obtener usuarios.' };
  }
};

export const updateUserRole = async (userId, newRole) => {
  try {
    const response = await loginService.put(`/${userId}`, { role: newRole });
    return { success: true, data: response.data };
  } catch (error) {
    console.error('Error al asignar el rol:', error);
    return { success: false, message: 'Error al asignar el rol.' };
  }
};

export const deleteUser = async (userId) => {
  try {
    const response = await loginService.delete(`/${userId}`); 
    return { success: true, data: response.data };
  } catch (error) {
    console.error('Error al eliminar el usuario:', error);
    return { success: false, message: 'Error al eliminar el usuario.' };
  }
};

export default loginService;