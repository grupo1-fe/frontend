import React, { useState, useEffect } from 'react';
import loginService, {
  getUsers as getUsersFromService,
  updateUserRole as updateUserRoleFromService,
  deleteUser as deleteUserFromService
} from '../../../../application/services/AuthService';
import './UserTable.scss';
import Modal from '../Modals/danger/Modal';
import ModalSuccess from '../Modals/success/ModalSuccess';
const UserTable = () => {
  const [users, setUsers] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [showModalSuccess, setShowModalSuccess] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);


  useEffect(() => {
    loadUsers();
  }, []);

  const loadUsers = async () => {
    try {
      const response = await getUsersFromService();
      setUsers(response.data);
    } catch (error) {
      console.error('Error loading users:', error);
    }
  };

  const assignRole = async (userId, newRole) => {
    try {
      await updateUserRoleFromService(userId, newRole);
      loadUsers();
    } catch (error) {
      console.error('Error assigning role:', error);
    }
  };


  const deleteUser = async (userId) => {
    try {
      setShowModal(false);
      await deleteUserFromService(userId);
      loadUsers();
      setShowModalSuccess(true);
      setSelectedUser(null);
      setTimeout(() => {
        setShowModalSuccess(false);
      }, 2000);
    } catch (error) {
      console.error('Error deleting user:', error);
    }
  };


  const alertDelete = (userId) => {
    const userToDelete = users.find(user => user.id === userId);
    setSelectedUser(userToDelete);
    setShowModal(true);
  };

  return (
    <div className="user-table w-full">
      <Modal
        title={'Borrar usuario'}
        text={`¿Estás seguro que deseas borrar al usuario ${selectedUser?.firstName} ${selectedUser?.lastName}?`}
        showModal={showModal}
        setShowModal={setShowModal}
        onClickAccept={() => deleteUser(selectedUser?.id)}
      />


      <ModalSuccess
        title={'Usuario borrado'}
        text={`Usuario borrado con éxito.`}
        showModalSuccess={showModalSuccess}
        setShowModalSuccess={setShowModalSuccess}
      />
      <h2 className="text-xl font-semibold mb-4">Usuarios</h2>
      <table className="w-full border-collapse">
        <thead>
          <tr className="bg-gray-200">
            <th className="py-2 px-4">ID</th>
            <th className="py-2 px-4">Nombre</th>
            <th className="py-2 px-4">Email</th>
            <th className="py-2 px-4">Rol</th>
            <th className="py-2 px-4">Modificar</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => (
            <tr key={user.id} className="border-t">
              <td className="py-2 px-4">{user.id}</td>
              <td className="py-2 px-4">{user.firstName} {user.lastName}</td>
              <td className="py-2 px-4">{user.email}</td>
              <td className="py-2 px-4">{user.role}</td>
              <td className="py-2 px-4">
                <button className="mr-2 px-2 py-1 bg-green-500 text-white rounded"
                  onClick={() => assignRole(user.id, 'admin')}>
                  Hacer Admin
                </button>
                <button className="mr-2 px-2 py-1 bg-blue-500 text-white rounded"
                  onClick={() => assignRole(user.id, 'user')}>
                  Hacer Usuario
                </button>
                <button className="px-2 py-1 bg-red-500 text-white rounded"
                  onClick={() => alertDelete(user.id)}>
                  Borrar
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default UserTable;
