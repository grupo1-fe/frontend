import './ModalWarning.scss'
import { AiOutlineCloseCircle } from 'react-icons/ai'

const ModalWarning = ({ title, text, showModal, setShowModal, onClickAccept }) => {
  const handleModalClick = (e) => {
    e.stopPropagation();
    setShowModal(false)
  };
  return (
    <article className={`modal-danger ${showModal ? 'show' : ''}`} role="alert" onClick={() => setShowModal(false)}>
      <span className="modal-close" aria-label="close modal" onClick={handleModalClick}><AiOutlineCloseCircle /></span>
      <h2 className="modal-title">
        {title}
      </h2>
      <p className='modal-text'>
        {text}
      </p>
      <section className='modal-actions'>
        <button className="modal-button-cancel" onClick={() => setShowModal(false)}>Cancelar</button>
        <button className="modal-button-accept" onClick={onClickAccept}>Aceptar</button>
      </section>
    </article>
  )
}

export default ModalWarning