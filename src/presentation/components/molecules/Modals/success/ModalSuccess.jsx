import './ModalSuccess.scss'
import { AiOutlineCloseCircle } from 'react-icons/ai'

const ModalSuccess = ({ title, text, showModalSuccess, setShowModalSuccess, onClickAccept }) => {
  const handleModalClick = (e) => {
    e.stopPropagation();
    setShowModalSuccess(false)
  };
  return (
    <article className={`modal-success ${showModalSuccess ? 'show' : ''}`} role="alert" onClick={() => setShowModalSuccess(false)}>
      <span className="modal-close" aria-label="close modal" onClick={handleModalClick}><AiOutlineCloseCircle /></span>
      <h2 className="modal-title">
        {title}
      </h2>
      <p className='modal-text'>
        {text}
      </p>
      <section className='modal-actions'>
        {/* <button className="modal-button-cancel" onClick={() => setShowModalSuccess(false)}>Cancelar</button> */}
        <button className="modal-button-accept" onClick={onClickAccept}>Aceptar</button>
      </section>
    </article>
  )
}

export default ModalSuccess