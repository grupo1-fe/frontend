import { useState, useEffect } from 'react'
import api from '../../../../adapters/api/axios';
import { useProductContext } from '../../../../context/ProductContext';
import { useCategoryContext } from '../../../../context/CategoryContext';
import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';
import './Table.scss'
import { Link } from 'react-router-dom';
import Modal from '../../molecules/Modals/danger/Modal';
import NoImgIcon from '../../../../assets/img/icon.png';


const Table = ({ product, handleDelete }) => {

  const { products, updateProducts } = useProductContext();
  const [showModal, setShowModal] = useState(false);
  const [id, setId] = useState(null);

  const alertDelete = (id) => {
    setId(id);
    setShowModal(true);
  }
  const { categories } = useCategoryContext();

  const handleCategoryChange = async (productId, newCategoryTitle) => {

    try {
      await api.put(`/products/${productId}`, { category: newCategoryTitle });
      const updatedProducts = products.map((product) => {
        if (product.id === productId) {
          return {
            ...product,
            category: {
              ...product.category,
              title: newCategoryTitle,
            }
          };
        }
        return product;
      });
      updateProducts(updatedProducts);

    } catch (error) {
      console.error('Error al actualizar la categoría del producto:', error);
    }
  };


  useEffect(() => {
    updateProducts(product);
  }, [category]);


  return (
    <>
      <table className="table" role="table">
        <thead>
          <tr>
            <th scope="col" className="article-th">
              img
            </th>
            <th scope="col" className="article-th">
              Id
            </th>
            <th scope="col" className="article-th">
              Nombre
            </th>
            <th scope="col" className="description-th" >
              Descripción
            </th>
            <th scope="col" className="description-th">
              Categoría
            </th>
            <th scope="col" className="price-th">
              Características
            </th>
            <th scope="col" className="price-th">
              Precio
            </th>
            <th scope="col" className="stock-th">
              Stock
            </th>
            <th scope="col" className="article-th">
              <Link to='/admin/agregar-categoria'>➕categoria</Link>
            </th>
          </tr>
        </thead>
        <tbody>
          {product.map((productD) => (
            <tr key={productD.id} className={`product-${productD.id}`}>
              <td className="image-td">
                <section className="image-td--article">
                  <figure className="image-td--article__figure">
                    <Link to={`/producto/${productD.id}`} className="relative block">
                      {productD.images.length > 0 ?
                        <img alt={productD.title} src={
                          productD.thumbnail.url} className="image-td--image" /> :
                        <img alt={productD.thumbnail.url} src={NoImgIcon} className="image-td--image" />
                      }
                    </Link>
                  </figure>
                </section>
              </td>
              <td className="price-td">
                <p>
                  {productD.id}
                </p>
              </td>
              <td className="article-td">
                <section>
                  <h4 className="product-title whitespace-no-wrap">
                    {productD.title}
                  </h4>
                </section>
              </td>
              <td className="description-td">
                <p className="description-p">
                  {productD.description}
                </p>
              </td>
              <td className='category-td'>
                <select
                  className='form-input'
                  name="productCategory"
                  id="category"
                  value={productD.category}
                  onChange={(e) => handleCategoryChange(productD.id, e.target.value)}
                >
                  {
                    categories.map((category) => (
                      <option key={category.id}
                        value={category.title}>
                        {category.title}
                      </option>
                    ))
                  }
                </select>
              </td>
              <td className="price-td">
                <p className="whitespace-pre-line">
                  {productD.characteristic.join(', ') || 'Sin características'
                  }
                </p>
              </td>
              <td className="price-td">
                <p className="whitespace-pre-line">
                  ${productD.price}
                </p>
              </td>
              <td className="stock-td">
                <span className={`relative inline-block px-3 py-1 font-semibold leading-tight ${productD.stock > 0 ? 'text-green-900' : 'text-red-900'}`}>
                  <span aria-hidden="true" className={`absolute inset-0 ${productD.stock > 0 ? 'bg-green-200' : 'bg-red-200'} rounded-full opacity-50`}>
                  </span>
                  <span className="relative">
                    {productD.stock > 0 ? productD.stock : 'Agotado'}
                  </span>
                </span>
              </td>

              <td className="actions-td">
                <ul className='actions-ul' >
                  <li>
                    <button aria-label="Editar producto">
                      <Link to={`/admin/editar-producto/${productD.id}`}>
                        <AiOutlineEdit />
                      </Link>
                    </button>
                  </li>
                  <li>
                    <button aria-label="Eliminar producto" onClick={
                      () => alertDelete(productD.id)}>
                      <AiOutlineDelete />
                    </button>
                  </li>
                </ul>
              </td>
            </tr>
          ))
          }
        </tbody>
      </table>

      <Modal
        title="Eliminar producto"
        text={`¿Estás seguro que deseas eliminar el producto id: ${id} ? `}
        showModal={showModal}
        setShowModal={setShowModal}
        handleDelete={handleDelete}
        onClickAccept={() => handleDelete(id)}
        id={id}
      />

    </>

  )
}

export default Table