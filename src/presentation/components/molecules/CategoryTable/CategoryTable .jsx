import { AiOutlineDelete } from 'react-icons/ai';
import './CategoryTable.scss';

const CategoryTable = ({ categories, handleDelete }) => {
  return (
    <div className="category-table">
      <table>
        <thead>
          <tr>
            <th>Nombre de la Categoría</th>
            <th>Descripción</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          {categories.map((category) => (
            <tr key={category.id}>
              <td>{category.title}</td>
              <td>{category.description}</td>
              <td>
                <button onClick={() => handleDelete(category.id)}>
                  <AiOutlineDelete />
                </button>

              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default CategoryTable;
