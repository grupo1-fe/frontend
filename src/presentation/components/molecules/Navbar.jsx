import { useState } from 'react';
import { AiOutlineMenu, AiOutlineMenuUnfold } from 'react-icons/ai';
import NavLink from '../atoms/NavLink/NavLink';
import Logo from '../atoms/Logo/Logo';
import Button from '../atoms/Button/Button';
import NavbarMenu from '../organisms/NabarMenu/NavbarMenu';
import './Navbar.scss';
import { useAuth } from '../../../context/AuthContext';
import { Link } from 'react-router-dom';
import Avatar from '../atoms/Avatar/Avatar';
const Navbar = () => {
  const [showMenu, setShowMenu] = useState(false);
  const { isAuthenticated, logout, user, setUser } = useAuth();


  const handleMenu = () => {
    setShowMenu(!showMenu);
  };

  const onClose = () => {
    setShowMenu(false);
    if (window.innerWidth <= 700) {
      setShowMenu(false);
    }
  }
  const links = [
    { to: '/admin/inicio', text: 'Admin' },
  ];

  return (
    <nav>
      <div className="navbar-header">
        <Logo />
        {showMenu ? <NavbarMenu links={links} isOpen={showMenu} onClose={onClose} user={user} logout={logout} setUser={setUser} isAuthenticated={isAuthenticated()}
        />
          : null}
      </div>
      <Button onClick={handleMenu} text={
        !showMenu ?
          <AiOutlineMenu /> : <AiOutlineMenuUnfold />
      } state="menu" />
      <div className="log-buttons">
        {isAuthenticated() ? (
          <>
            {user ? <Avatar user={user} logout={logout}  onClick={handleMenu} showMenu={showMenu} /> : null}
          </>
        ) : (
          <>
            <Button text={
              <Link to="/login">Iniciar sesión</Link>
            } state="signIn" />
            <Button text={
              <Link to="/register">Crear cuenta</Link>
            } state="signUp" />
          </>
        )
        }
      </div>
    </nav>
  );
};

export default Navbar;