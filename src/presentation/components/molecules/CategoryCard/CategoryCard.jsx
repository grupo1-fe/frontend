import './CategoryCard.scss'
import { Link } from 'react-router-dom'
import { useCategoryContext } from '../../../../context/CategoryContext'

const CategoryCard = () => {

  const { categories } = useCategoryContext()

  return (
    <>
      <section className='category-grid'>

        {categories.map((category) => (
          <article className="category-card" key={category.id}>
            <Link to={`/category/${category.id}`}>
              <section className="category-section">
                <figure>
                  <img className="category-img" src={category.icon.url} alt={category.title} />
                </figure>
                <figcaption className="category-title">{category.title}</figcaption>
                <p className="category-description">{category.description}</p>
              </section>
            </Link>
          </article>
        ))}
      </section>
    </>
  )
}

export default CategoryCard