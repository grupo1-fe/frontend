import './CategoryFilter.scss';

const CategoryFilter = ({ products, onCategoryChange }) => {

  const uniqueCategories = [...new Set(products.map(product => product.category))];

  return (
    <div className="category-filter">
      <h3 className="category-filter-title">Filtrar por categoría</h3>
      <ul className="category-filter-list">
        {uniqueCategories.map((categoryTitle, index) => (
          <li key={index} className="category-filter-list-item">
            <input
              type="checkbox"
              name={categoryTitle}
              id={categoryTitle}
              onChange={() => onCategoryChange(categoryTitle)}
            />
            <label htmlFor={categoryTitle}>{categoryTitle}</label>
          </li>
        ))}
      </ul>
    </div>
  )
}

export default CategoryFilter;
