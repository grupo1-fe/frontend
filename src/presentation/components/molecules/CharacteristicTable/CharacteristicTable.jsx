import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';
import { useState, useRef } from 'react';
import { useCharacteristicsContext } from '../../../../context/CharacteristicsContext';
import apiCharacteristics from '../../../../application/services/Characteristics';
import { useProductContext } from '../../../../context/ProductContext';
import './CharacteristicTable.scss';
const CharacteristicTable = ({ characteristics, handleDelete }) => {
  const [editingCharacteristic, setEditingCharacteristic] = useState(null);
  const { updateCharacteristics, updateUpdatedCharacteristics } = useCharacteristicsContext();
  const { products, updateProducts } = useProductContext();
  const editInputRef = useRef(null);

  const handleEdit = async (characteristicId) => {
    const updatedTitle = editInputRef.current.value;

    try {
      const response = await apiCharacteristics.put(`/characteristic/${characteristicId}`, {
        title: updatedTitle
      });

      const updatedCharacteristics = characteristics.map((characteristic) => {
        if (characteristic.id === characteristicId) {
          return {
            ...characteristic,
            title: updatedTitle
          };
        }
        return characteristic;
      });

      updateCharacteristics(updatedCharacteristics);

      const productsWithUpdatedCharacteristic = products.map((product) => {
        const updatedProductCharacteristics = product.characteristic.map((char) => {
          console.log('char', char);
          if (char === characteristicId) {
            console.log('char', char, 'characteristicId', characteristicId);
            return updatedTitle;
          }
          return char;
        });

        return {
          ...product,
          characteristic: updatedProductCharacteristics,
        };
      });

      updateProducts(productsWithUpdatedCharacteristic);

      updateUpdatedCharacteristics(characteristicId, updatedTitle);
    } catch (error) {
      console.error("Error al actualizar la característica", error);
    }

    setEditingCharacteristic(null);
  };


  return (
    <div>
      <table className='table w-full' role='table'>
        <thead className='table-header'>
          <tr className='table-row'>
            <th className='mr-12'>Icono</th>
            <th className='mr-12'>Nombre de la Característica</th>
            <th className='mr-12'>Acciones</th>
          </tr>
        </thead>
        <tbody>
          {characteristics.map((characteristic) => (
            <tr key={characteristic.id}>
              <td>
                <img src={characteristic.icon.url} alt={characteristic.title} className='mx-auto object-cover rounded-full h-10 w-10' />
              </td>
              {editingCharacteristic === characteristic.id ? (
                <>
                  <td>
                    <input type="text" defaultValue={characteristic.title} id={`edit-${characteristic.id}`} ref={editInputRef} />
                  </td>
                  <td>
                    <button onClick={() => {
                      const updatedTitle = document.getElementById(`edit-${characteristic.id}`).value;
                      handleEdit(characteristic.id, { title: updatedTitle });
                    }}>
                      Guardar
                    </button>
                    <button onClick={() => setEditingCharacteristic(null)}>
                      Cancelar
                    </button>
                  </td>
                </>
              ) : (
                <>
                  <td>{characteristic.title}</td>
                  <td>
                    <button onClick={() => handleDelete(characteristic.id)}>
                      <AiOutlineDelete />
                    </button>
                    <button onClick={() => setEditingCharacteristic(characteristic.id)}>
                      <AiOutlineEdit />
                    </button>
                  </td>
                </>
              )}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );

}

export default CharacteristicTable