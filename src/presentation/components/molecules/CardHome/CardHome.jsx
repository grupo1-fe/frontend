import { Link } from 'react-router-dom'
import './CardHome.scss'
import NoImg from '../../../../assets/img/no-img.png'

const CardHome = ({ product }) => {
  const { title, description, thumbnail, images } = product

  return (
    <article className="cardHome">
      <figure>
        <Link to={`/producto/${product.id}`} className='card-link'>
          {
            images.length > 0 ?
              <img src={thumbnail.url} alt={title} className="cardImg" /> : <img src={NoImg} alt={title} className="no-img" />
          }
        </Link>
      </figure>
      <section className="section-data">
        {
          <span className='category-badge'>
            {product.category}
          </span>
        }
        <Link to={`/producto/${product.id}`}>
          <p className="title">
            {title}
          </p>
          <p className="description">
            {description}
          </p>
        </Link>
      </section>

    </article>
  )
}

export default CardHome