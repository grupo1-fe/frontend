import './Card.scss';

import NoImg from '../../../../assets/img/no-img.png';
const Card = ({ product }) => {

  const {
    title,
    description,
    price,
    thumbnail,
    images
  } = product;

  return (
    <article className="card">
      <figure className="card-image">
        {
          images.length > 0 ?
            <img src={thumbnail.url} alt={title} className='main-card-img' />
            :
            <img src={NoImg} alt={title} className='main-card-img' />
        }
      </figure>
      <section className="card-content">
        <h3 className="card-title">{title}</h3>
      </section>
    </article>
  );
};

export default Card;