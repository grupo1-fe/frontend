import './MainLayout.scss';
const MainLayout = ({ children }) => {
  return (
    <main className='main-layout'>
      {children}
    </main>

  );
};

export default MainLayout;
