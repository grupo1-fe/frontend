import './Logo.scss';
import { Link } from 'react-router-dom';
import Lema from '../../../../assets/img/lema.svg';
import logo from '../../../../assets/img/logo.svg';
const Logo = () => {
  return (
    <Link to="/" className='logo'>
      <img src={logo} alt="logo"  className='logoImg'/>
      <img src={Lema} alt="lema" className='lemaImg' />
    </Link>
  );
};

export default Logo;