import './Avatar.scss'
import { Link } from 'react-router-dom';

const Avatar = ({ user, logout, showMenu, onClick }) => {

  // Si no hay usuario o es un array vacío, retornamos null
  if (!user || user.length === 0) {
    return null;
  }

  const { firstName, lastName, role } = user;

  return (
    <article className='avatar'>
      <span className='avatar-text' onClick={onClick}>
        {firstName[0] + lastName[0]}
      </span>

      {showMenu && (
        <ul className='user-actions'>
          {role === 'admin' && (
            <li className='user-action'>
              <Link to='/admin/inicio' className='user-action-text'>Admin</Link>
            </li>
          )}
          <li onClick={logout} className='logout-text'>Cerrar sesión</li>
        </ul>
      )}
    </article>
  )
}

export default Avatar;
