import './Button.scss';

const Button = ({ text, state, onClick }) => {
  return (
    <button onClick={onClick} className={`button button--${state}`}>
      {text}
    </button>
  );
};

export default Button;