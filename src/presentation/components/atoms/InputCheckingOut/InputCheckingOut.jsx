import { useState, useEffect } from 'react';
import DatePicker, { registerLocale } from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import es from 'date-fns/locale/es';
import './InputCheckingOut.scss';

registerLocale('es', es);
const InputCheckingOut = () => {
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(null);

  useEffect(() => {
    const savedStartDate = localStorage.getItem('startDate');
    const savedEndDate = localStorage.getItem('endDate');

    if (savedStartDate) setStartDate(new Date(savedStartDate));
    if (savedEndDate) setEndDate(new Date(savedEndDate));
  }, []);

  const onChange = (dates) => {
    const [start, end] = dates;
    setStartDate(start);
    setEndDate(end);

    if (start) localStorage.setItem('startDate', start.toString());
    if (end) localStorage.setItem('endDate', end.toString());
  };

  return (
    <>
      <DatePicker
        className="input-checking-out"
        selected={startDate}
        onChange={onChange}
        startDate={startDate}
        endDate={endDate}
        selectsRange
        locale={es}
        dateFormat={'dd/MM/yyyy'}
      />
    </>
  );
};

export default InputCheckingOut;

