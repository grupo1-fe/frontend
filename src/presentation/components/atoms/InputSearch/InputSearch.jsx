import './InputSearch.scss';

const InputSearch = ({ value, onChange, placeholder, type, name }) => {
  return (
    < >
      <input 
        type={type}
        value={value}
        onChange={(e) => onChange(e.target.value)}
        placeholder={placeholder}
        name={name}
        className="search" />
    </>
  );
};

export default InputSearch;
