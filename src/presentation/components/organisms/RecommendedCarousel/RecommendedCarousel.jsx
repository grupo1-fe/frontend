import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import Card from '../../molecules/Card/Card';
import { Link } from 'react-router-dom';
import './RecommendedCarousel.scss';
const RecommendedCarousel = ({ displayedProducts }) => {
  return (
    <Carousel showThumbs={false} showStatus={false} showIndicators={true} swipeable={true} dynamicHeight={true}
      labels={{ leftArrow: 'Anterior', rightArrow: 'Siguiente', item: 'Producto' }}
    >
      {displayedProducts.map((product) => (
        <div key={product.id}>
          <Link to={`/producto/${product.id}`}>
            <Card product={product} />
          </Link>
        </div>
      ))}
    </Carousel>
  );
};

export default RecommendedCarousel;