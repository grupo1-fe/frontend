import React, { useState } from 'react';
import InputSearch from '../../atoms/InputSearch/InputSearch';
import InputCheckingOut from '../../atoms/InputCheckingOut/InputCheckingOut';
import './Searcher.scss';

const Searcher = () => {
  const [searchValue, setSearchValue] = useState('');
  const handleSearchChange = (value) => {
    setSearchValue(value);
  };


  return (
    <section className='searcher'>
      <h2 className='section-title'>¿Qué te gustaría hacer?</h2>
      <div className='input-container'>
        <section className="search-item">
          <InputSearch
            type='search'
            placeholder='Buscar'
            value={searchValue}
            onChange={handleSearchChange}
            name={'search'}
          />
        </section>
        <section className="search-date">
          <InputCheckingOut />
        </section>
      </div>
    </section>
  );
};

export default Searcher;