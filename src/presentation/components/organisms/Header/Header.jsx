import './Header.scss'
import Navbar from '../../molecules/Navbar';


const Header = () => {
  return (
    <header>
      <Navbar />
    </header>
  );
};

export default Header;