import { Link } from 'react-router-dom';
import './NavbarMenu.scss';
import Button from '../../atoms/Button/Button';
import Avatar from '../../atoms/Avatar/Avatar';

const NavbarMenu = ({ links, isOpen, onClose, logout, isAuthenticated, user }) => {
  return (
    <>
      <ul className={`navbar-menu ${isOpen ? 'active' : ''}`}>
        <Avatar user={user} logout={logout} />
        {links.map((link) => (
          <li key={link.to} onClick={onClose}>
            {
              isAuthenticated ? <Link to={link.to}>{link.text}</Link> : null
            }
          </li>
        ))}
        {isAuthenticated ? (
          <Button onClick={logout} text="Cerrar sesión" state="signIn" />
        ) : (
          <>
            <Button text={
              <Link to="/login">Iniciar sesión</Link>
            } state="signIn" />
            <Button text={
              <Link to="/register">Crear cuenta</Link>
            } state="signUp" />
          </>
        )}

      </ul>
    </>
  );
};

export default NavbarMenu;
