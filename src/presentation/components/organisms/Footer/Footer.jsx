import './Footer.scss';
import Logo from '../../atoms/Logo/Logo';
import { SlSocialInstagram, SlSocialFacebook, SlSocialYoutube } from "react-icons/sl";
const Footer = () => {
  return (
    <footer className="footer">
      <article className="footer-container">
        <section className="footer-container__section">
          <span className="footer-logo">
            <Logo />
          </span>
          <p className="footer-copy">
            &copy;  Desarrollado por Ruta Viva - <span>{new Date().getFullYear()}</span>
          </p>
        </section>
        <section className="footer-social">
          <a rel="noreferrer">
            <SlSocialYoutube />
          </a>
          <a rel="noreferrer">
            <SlSocialFacebook />
          </a>
          <a rel="noreferrer">
            <SlSocialInstagram />
          </a>
        </section>
      </article>
    </footer>
  );
};

export default Footer;
