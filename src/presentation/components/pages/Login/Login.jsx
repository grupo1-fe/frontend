import { useState } from 'react';
import './Login.scss';
import NoImg from '../../../../assets/img/icon.png';
import { useNavigate } from 'react-router-dom';
import { loginUser } from '../../../../application/services/AuthService';
import { useAuth } from '../../../../context/AuthContext';
import { Link } from 'react-router-dom';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const navigate = useNavigate();

  const { refreshUser } = useAuth();


  const handleLogin = async (event) => {
    event.preventDefault();

    if (!validateForm()) {
      return;
    }

    try {
      setIsLoading(true);
      const response = await loginUser(email, password);
      if (response.success) {
        localStorage.setItem('user', JSON.stringify(response.data));
        refreshUser();
        navigate('/');
      } else {
        setErrorMessage('Correo o contraseña incorrectos.');
      }

    } catch (error) {
      setErrorMessage(error.response?.data?.message || 'Error en el inicio de sesión. Verifica tus credenciales.');
      console.error('Error en el inicio de sesión:', error.response?.data?.message || error.message);
    } finally {
      setIsLoading(false);
    }
  };
 
  const validateForm = () => {
    if (!email || !password) {
      setErrorMessage('Por favor, ingresa correo y contraseña.');
      return false;
    }

    if (!validateEmail(email)) {
      setErrorMessage('Ingresa un correo electrónico válido.');
      return false;
    }

    if (!validatePassword(password)) {
      setErrorMessage('La contraseña debe tener al menos 6 caracteres.');
      return false;
    }

    setErrorMessage('');
    return true;
  };

  const validateEmail = (email) => {
    const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailRegex.test(email);
  };

  const validatePassword = (password) => {
    return password.length >= 6;
  };
  return (
    <section className="login">
      <article className="login-card">
        <img className="w-14" src={NoImg} alt="logo" />
        <h1 className="self-center mb-2 text-xl font-light text-gray-800 sm:text-2xl">Iniciar sesión</h1>
        <form onSubmit={handleLogin} className='form'>
          <input
            className="input-email"
            placeholder="Correo"
            name="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            autoComplete="off"
          />
          <input
            className="input-password"
            placeholder="Contraseña"
            type="password"
            name="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            autoComplete="off"
          />
          {errorMessage && <p className="error-message">{errorMessage}</p>}
          <button className="btn-login" type="submit" disabled={isLoading}>
            {isLoading ? 'Cargando...' : 'Iniciar sesión'}
          </button>
        </form>
        <p className="text-sm hover:teal-blue-700 ml-3">¿No tienes cuenta? <Link className="text-teal-700" to="/register">Regístrate</Link></p>
      </article>
    </section>
  );
};

export default Login;