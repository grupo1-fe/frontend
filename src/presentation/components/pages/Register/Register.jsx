import React, { useState } from 'react';
import './Register.scss';
import { Link, useNavigate } from 'react-router-dom';
import { useAuth } from '../../../../context/AuthContext';


const Register = () => {
  const { register } = useAuth();
  const navigate = useNavigate();

  const [userDetails, setUserDetails] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: '',
    role: 'user',
  });

  const [registerInputErrors, setRegisterInputErrors] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: '',
  });

  const [errorMessage, setErrorMessage] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!validateForm()) {
      return;
    }

    const response = await register(userDetails);
    if (response.success) {
      navigate('/login');
    } else {
      setErrorMessage(response.message);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUserDetails((prevDetails) => ({
      ...prevDetails,
      [name]: value,
    }));
  };

  const validateEmail = (email) => {
    const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailRegex.test(email);
  };

  const validatePassword = (password) => {
    return password.length >= 6;
  };

  const validateForm = () => {
    let errors = {};
    let formIsValid = true;

    if (!userDetails.firstName) {
      formIsValid = false;
      errors.firstName = 'El nombre es requerido.';
    }

    if (!userDetails.lastName) {
      formIsValid = false;
      errors.lastName = 'El apellido es requerido.';
    }

    if (!userDetails.email) {
      formIsValid = false;
      errors.email = 'El correo es requerido.';
    } else if (!validateEmail(userDetails.email)) {
      formIsValid = false;
      errors.email = 'Ingresa un correo electrónico válido.';
    }

    if (!userDetails.password) {
      formIsValid = false;
      errors.password = 'La contraseña es requerida.';
    } else if (!validatePassword(userDetails.password)) {
      formIsValid = false;
      errors.password = 'La contraseña debe tener al menos 6 caracteres.';
    }

    if (userDetails.password !== userDetails.confirmPassword) {
      formIsValid = false;
      errors.confirmPassword = 'Las contraseñas no coinciden.';
    }

    setRegisterInputErrors(errors);

    return formIsValid;
  };

  return (
    <section className='register-section'>
      <h2>Crear cuenta</h2>
      <span>
        <p>¿Ya tienes una cuenta?
          <Link to='/login' target="_blank" className="text-sm text-teal-500 underline hover:teal-blue-700 ml-3">
            Inicia sesión
          </Link>
        </p>
      </span>
      <div className="p-6 mt-8">
        <form onSubmit={handleSubmit}>
          <div className="flex gap-4 mb-2">
            <label htmlFor='firstName'>
              <input
                type="text"
                name="firstName"
                value={userDetails.firstName}
                onChange={handleChange}
                placeholder="Nombre"
                autoComplete="off"
              />
              {registerInputErrors.firstName && <p className="error-message">{registerInputErrors.firstName}</p>}
            </label>
            <label htmlFor='lastName'>
              <input
                type="text"
                name="lastName"
                value={userDetails.lastName}
                onChange={handleChange}
                placeholder="Apellido"
                autoComplete="off"
              />
              {registerInputErrors.lastName && <p className="error-message">{registerInputErrors.lastName}</p>}
            </label>

          </div>
          <div className="flex flex-col mb-2">
            <label htmlFor='email'>
              <input
                type="text"
                name="email"
                value={userDetails.email}
                onChange={handleChange}
                placeholder="Email"
                autoComplete="off"
              />
            </label>
            {registerInputErrors.email && <p className="error-message">{registerInputErrors.email}</p>}
          </div>
          <div className="flex flex-col mb-2">
            <label htmlFor='password'>
              <input
                type="password"
                name="password"
                value={userDetails.password}
                onChange={handleChange}
                placeholder="Contraseña"
                autoComplete="off"
              />
            </label>
            {registerInputErrors.password && <p className="error-message">{registerInputErrors.password}</p>}
          </div>
          <div className="flex flex-col mb-2">
            <label htmlFor='confirmPassword'>
              <input
                type="password"
                name="confirmPassword"
                value={userDetails.confirmPassword}
                onChange={handleChange}
                placeholder="Confirmación de contraseña"
                autoComplete="off"
              />
            </label>
            {registerInputErrors.confirmPassword && <p className="error-message">{registerInputErrors.confirmPassword}</p>}
          </div>
          <div className="flex w-full my-4 justify-center">
            <button type="submit">
              Registrarse
            </button>
          </div>
        </form>
        {errorMessage && (
          <p className="error-message">{errorMessage}</p>
        )}
      </div>
    </section>
  );
};

export default Register;
