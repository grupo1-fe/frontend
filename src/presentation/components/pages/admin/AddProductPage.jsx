import { useState } from 'react';
import { useProductContext } from '../../../../context/ProductContext';
import { useAuth } from '../../../../context/AuthContext';
import api from '../../../../adapters/api/axios';
import Button from '../../atoms/Button/Button'
import './AddProductPage.scss';
import ModalSuccess from '../../molecules/Modals/success/ModalSuccess';
import apiCategory from '../../../../application/services/Category';
import { useCategoryContext } from '../../../../context/CategoryContext';
import { useCharacteristicsContext } from '../../../../context/CharacteristicsContext';


const AddProductPage = () => {
  const [imageFiles, setImageFiles] = useState([]);
  const { user } = useAuth();
  const { categories } = useCategoryContext();
  const { characteristics } = useCharacteristicsContext();
  const [category, setCategory] = useState({
    title: '',
    description: '',
    icon: '',
  });

  const [characteristic, setCharacteristic] = useState([]);


  const [productData, setProductData] = useState({
    productName: '',
    productDescription: '',
    productPrice: '',
    productStock: '',
    productCategory: category,
    productCharacteristic: characteristic,
    createdBy: user.email,
  });

  const {
    productName,
    productDescription,
    productPrice,
    productStock,
    createdBy,
    productCategory,
    productCharacteristic,
  } = productData;

  const [formErrors, setFormErrors] = useState({});
  const [showModalSuccess, setShowModalSuccess] = useState(false);
  const { products, updateProducts } = useProductContext();


  const validateForm = () => {
    const errors = {};
    !productName.trim() && (errors.productName = 'El nombre del producto es requerido');

    if (productName.trim().length < 3) {
      errors.productName = 'El nombre del producto debe tener al menos 3 caracteres';
    } else if (productName.trim().length > 50) {
      errors.productName = 'El nombre del producto debe tener menos de 50 caracteres';
    } else if (!/^[a-zA-Z0-9 ]+$/.test(productName.trim())) {
      errors.productName = 'El nombre del producto solo puede contener letras, números y espacios';
    } else if (productName.trim().includes('  ')) {
      errors.productName = 'El nombre del producto no puede contener espacios consecutivos';
    }

    productName.trim() && checkProductName(productName) && (errors.productName = 'El nombre del producto ya existe');

    !productDescription.trim() && (errors.productDescription = 'La descripción del producto es requerida');


    if (!productPrice) {
      errors.productPrice = 'El precio del producto es requerido';
    } else if (isNaN(parseFloat(productPrice))) {
      errors.productPrice = 'El precio debe ser un número válido';
    } else if (parseFloat(productPrice) < 0) {
      errors.productPrice = 'El precio debe ser mayor a 0';
    } else if (parseFloat(productPrice) > 999999) {
      errors.productPrice = 'El precio debe ser menor a 999999';
    }


    !productStock && (errors.productStock = 'El stock del producto es requerido');

    if (productStock && isNaN(parseInt(productStock))) {
      errors.productStock = 'El stock debe ser un número válido';
    } else if (parseInt(productStock) < 0) {
      errors.productStock = 'El stock debe ser mayor a 0';
    } else if (parseInt(productStock) > 999999) {
      errors.productStock = 'El stock debe ser menor a 999999';
    } else if (parseInt(productStock) % 1 !== 0) {
      errors.productStock = 'El stock debe ser un número entero';
    }

    setFormErrors(errors);
    return Object.keys(errors).length === 0;
  }

  const checkProductName = (name) => {
    const isNameTaken = products.some(product => product.title === name);
    return isNameTaken;
  };


  const handleInputChange = (event) => {
    const { name, value, checked, type } = event.target;

    if (name === "productCharacteristic") {
      if (checked) {
        setCharacteristic(prevCharacteristics => [...prevCharacteristics, value]);
      } else {
        setCharacteristic(prevCharacteristics => prevCharacteristics.filter(item => item !== value));
      }
    } else if (name.startsWith("category.")) {
      const field = name.split(".")[1];
      setCategory(prevCategory => ({
        ...prevCategory,
        [field]: value,
      }));
      setProductData(prevData => ({
        ...prevData,
        productCategory: {
          ...prevData.productCategory,
          [field]: value,
        },
      }));
    } else {
      setProductData(prevData => ({
        ...prevData,
        [name]: value,
      }));
    }
  };



  const handleImageUpload = (event) => {
    const imageFiles = Array.from(event.target.files);

    const updatedImageFiles = [];


    imageFiles.forEach(image => {
      const reader = new FileReader();
      reader.onloadend = () => {
        image.url = reader.result;
      };
      reader.readAsDataURL(image);

      updatedImageFiles.push(image);
    });

    setImageFiles(updatedImageFiles);
  };


  const resetForm = () => {
    setProductData({
      productName: '',
      productDescription: '',
      productPrice: '',
      productStock: '',
      productCategory: '',
      productCharacteristic: '',
    });
    setImageFiles([]);
    setFormErrors({});
  };

  const handleAddProduct2 = async () => {

    if (!validateForm()) {
      return;
    }

    const formData = new FormData();
    formData.append('title', productName);
    formData.append('description', productDescription);
    formData.append('price', parseFloat(productPrice));
    formData.append('stock', parseInt(productStock));
    formData.append('category', productCategory);
    characteristic.forEach((char, index) => {
      formData.append(`characteristic[${index}]`, char);
    });
    formData.append('thumbnail', imageFiles[0]);
    formData.append('createdBy', createdBy);
    imageFiles.forEach((image, index) => {
      formData.append(`images[${index}]`, image);
    });

    try {
      const response = await api.post('/products', formData);
      console.log('Respuesta del servidor:', response.data);
      updateProducts([...products, response.data]);
      setShowModalSuccess(true);
      setTimeout(() => {
        setShowModalSuccess(false);
      }, 2000);
      resetForm();

    } catch (error) {
      console.error('Error al subir el producto:', error);
    }
  };


  return (
    <>
      <div className="add-product">
        <ModalSuccess title="Producto agregado" text="El producto se agregó correctamente" showModalSuccess={showModalSuccess} setShowModalSuccess={setShowModalSuccess} />
        <h2 className='page-title'>Agregar Nuevo Producto</h2>
        <form action="" className='add-product__form'>
          <label className='form-label'>Nombre del producto</label>
          <input className='form-input' type="text" value={productName} onChange={handleInputChange} name='productName' autoComplete="off" />
          {formErrors.productName && <p className="error-message">{formErrors.productName}</p>}
          <label className='form-label'>Descripción del producto</label>
          <textarea className='form-input' value={productDescription} onChange={handleInputChange} name='productDescription' autoComplete="off" />
          {formErrors.productDescription && <p className="error-message">{formErrors.productDescription}</p>}
          <label className='form-label'>Precio del producto</label>
          <input className='form-input' type="number" value={productPrice} onChange={handleInputChange} name='productPrice' />
          {formErrors.productPrice && <p className="error-message">{formErrors.productPrice}</p>}
          <label className='form-label'>Imagen del producto</label>
          <input className='form-input' type="file" multiple onChange={handleImageUpload} name='images' accept=".jpg, .jpeg, .png, .webp" />
          <label className='form-label'>Stock del producto</label>
          <input className='form-input' type="number" value={productStock} onChange={handleInputChange} name='productStock' />
          {formErrors.productStock && <p className="error-message">{formErrors.productStock}</p>}
          <label className='form-label'>Categoría del producto</label>
          <select className='form-input' name="productCategory" id="category" onChange={handleInputChange} value={productCategory}>
            <option value="">Elija una categoría</option>
            {categories.map((category) => (
              <option key={category.id} value={category.title}>
                {category.title}
              </option>
            ))}
          </select>

          <label className='form-label'>Características del producto</label>
          <ul>
            {characteristics.map((characteristic) => (
              <li key={characteristic.id}>
                <input type="checkbox" name="productCharacteristic" id={characteristic.title} value={characteristic.title} onChange={handleInputChange} />
                <label htmlFor={characteristic.title}>{characteristic.title}</label>
              </li>
            ))}
          </ul>
        </form>
        <Button type="button" onClick={handleAddProduct2} text="Agregar Producto" state="signUp" />
      </div>
      <h3 className='preview-title'>Vista previa</h3>
      <section className='image-preview__container'>
        {imageFiles.map((image, index) => (
          <figure key={index} className='image-preview'>
            <img src={image.url}
              alt={image.url} />
          </figure>
        ))
        }
      </section>
    </>
  );
};

export default AddProductPage;
