import { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { useProductContext } from '../../../../../context/ProductContext';
import api from '../../../../../adapters/api/axios';
import Button from '../../../atoms/Button/Button';
import './AdminEditProductPage.scss';
import ModalSuccess from '../../../molecules/Modals/success/ModalSuccess';



const AdminEditProductPage = () => {
  const { products, updateProducts } = useProductContext();
  const { productId } = useParams();
  const [productData, setProductData] = useState({
    productName: '',
    productDescription: '',
    productPrice: Number,
    productStock: Number,
    productCategory: '',
  });


  const {
    productName,
    productDescription,
    productPrice,
    productStock,
    productCategory,
  } = productData;



  const navigate = useNavigate();
  const [showModalSuccess, setShowModalSuccess] = useState(false);
  const [formErrors, setFormErrors] = useState({});
  const [imageFiles, setImageFiles] = useState([]);

  const validateForm = () => {
    const errors = {};
    !productName.trim() && (errors.productName = 'El nombre del producto es requerido');

    if (productName.trim().length < 3) {
      errors.productName = 'El nombre del producto debe tener al menos 3 caracteres';
    } else if (productName.trim().length > 50) {
      errors.productName = 'El nombre del producto debe tener menos de 50 caracteres';
    } else if (!/^[a-zA-Z0-9 ]+$/.test(productName.trim())) {
      errors.productName = 'El nombre del producto solo puede contener letras, números y espacios';
    } else if (productName.trim().includes('  ')) {
      errors.productName = 'El nombre del producto no puede contener espacios consecutivos';
    }

    productName.trim() && checkProductName(productName) && (errors.productName = 'El nombre del producto ya existe');

    !productDescription.trim() && (errors.productDescription = 'La descripción del producto es requerida');


    if (!productPrice) {
      errors.productPrice = 'El precio del producto es requerido';
    } else if (isNaN(parseFloat(productPrice))) {
      errors.productPrice = 'El precio debe ser un número válido';
    } else if (parseFloat(productPrice) < 0) {
      errors.productPrice = 'El precio debe ser mayor a 0';
    } else if (parseFloat(productPrice) > 999999) {
      errors.productPrice = 'El precio debe ser menor a 999999';
    }


    !productStock && (errors.productStock = 'El stock del producto es requerido');

    if (productStock && isNaN(parseInt(productStock))) {
      errors.productStock = 'El stock debe ser un número válido';
    } else if (parseInt(productStock) < 0) {
      errors.productStock = 'El stock debe ser mayor a 0';
    } else if (parseInt(productStock) > 999999) {
      errors.productStock = 'El stock debe ser menor a 999999';
    } else if (parseInt(productStock) % 1 !== 0) {
      errors.productStock = 'El stock debe ser un número entero';
    }

    setFormErrors(errors);
    return Object.keys(errors).length === 0;
  }

  const checkProductName = (name) => {
    const isNameTaken = products.some(product => product.title === name);
    return isNameTaken;
  };
  useEffect(() => {
    const fetchProductDetails = async () => {
      try {
        const response = await api.get(`/products/${productId}`);
        const productDetails = response.data;
        setProductData(productDetails);
      } catch (error) {
        console.error('Error al obtener detalles del producto:', error);
      }
    };

    fetchProductDetails();
  }, [productId]);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setProductData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleImageUpload = (event) => {
    const imageFiles = Array.from(event.target.files);

    const updatedImageFiles = [];


    imageFiles.forEach(image => {
      const reader = new FileReader();
      reader.onloadend = () => {
        image.url = reader.result;
      };
      reader.readAsDataURL(image);

      updatedImageFiles.push(image);
    });

    setImageFiles(updatedImageFiles);
  };



  const handleUpdateProduct = async () => {
    if (!validateForm()) {
      return;
    }

    const updatedProduct = {
      title: productName,
      description: productDescription,
      price: productPrice,
      stock: productStock,
      category: productCategory,
      images: imageFiles,
      thumbnail: imageFiles[0],
    };


    try {
      await api.put(`/products/${productId}`, updatedProduct);
      updateProducts(products.map(product => product.id === productId ? updatedProduct : product));

      setShowModalSuccess(true);
      setTimeout(() => {
        setShowModalSuccess(false);
      }, 2000);
    } catch (error) {
      console.error('Error al actualizar el producto:', error);
    }
  };
  // console.log('productData', productData);

  return (
    <>
      <div className="add-product">
        <ModalSuccess title="Producto agregado" text="El producto se editó correctamente" showModalSuccess={showModalSuccess} setShowModalSuccess={setShowModalSuccess} />
        <h2 className='page-title'>Editar {productData.title}</h2>
        <form action="" className='add-product__form'>
          <label className='form-label'>Nombre del producto</label>
          <input className='form-input' type="text" value={productName} onChange={handleInputChange} name='productName' autoComplete="off" placeholder={productData.title} />
          {formErrors.productName && <p className="error-message">{formErrors.productName}</p>}
          <label className='form-label'>Descripción del producto</label>
          <textarea className='form-input' value={productDescription} onChange={handleInputChange} name='productDescription' autoComplete="off" placeholder={productData.description} />
          {formErrors.productDescription && <p className="error-message">{formErrors.productDescription}</p>}
          <label className='form-label'>Precio del producto</label>
          <input className='form-input' type="number" value={productPrice} onChange={handleInputChange} name='productPrice' placeholder={productData.price} />
          {formErrors.productPrice && <p className="error-message">{formErrors.productPrice}</p>}
          <label className='form-label'>Imagen del producto</label>
          <input className='form-input' type="file" multiple onChange={handleImageUpload} name='images' accept=".jpg, .jpeg, .png, .webp" />
          <label className='form-label'>Stock del producto</label>
          <input className='form-input' type="number" value={productStock} onChange={handleInputChange} name='productStock' placeholder={productData.stock} />
          {formErrors.productStock && <p className="error-message">{formErrors.productStock}</p>}
          <label className='form-label'>Categoría del producto</label>
          <select className='form-input' name="productCategory" id="category" onChange={handleInputChange} value={productCategory} >
          {
                    products.map((product) => (
                      <option key={product.id}
                        value={product.category.title}>
                        {product.category.title}
                      </option>
                    ))
                  }
          </select>
        </form>
        <Button type="button" onClick={handleUpdateProduct} text="Agregar Producto" state="signUp" />
      </div>
      <h3 className='preview-title'>Vista previa</h3>
      <section className='image-preview__container'>
        {imageFiles.map((image, index) => (
          <figure key={index} className='image-preview'>
            <img src={image.url}
              alt={image.url} />
          </figure>
        ))
        }
      </section>
    </>
  );
};

export default AdminEditProductPage;
