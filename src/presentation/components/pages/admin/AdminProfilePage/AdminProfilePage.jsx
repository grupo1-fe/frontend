import { useAuth } from "../../../../../context/AuthContext";
import { useNavigate } from 'react-router-dom';
import './AdminProfile.scss';
import UserTable from "../../../molecules/UserTable/UserTable";

const AdminProfilePage = () => {
  const { user } = useAuth();
  const navigate = useNavigate();

  if (!user || user.role !== 'admin') {
    return (
      <div className='admin-page h-screen justify-center'>
        <h1 className='font-bold text-xl'>Error 403</h1>
        <button onClick={() => navigate('/')} className='btn btn-primary shadow-sm text-emerald-500'>Volver al inicio</button>
      </div>
    )
  }

  return (
    <section className='admin-profile-section'>
      <h2 className="self-center mb-2 text-xl font-light text-gray-800 sm:text-2xl">
        Perfil de Administrador
      </h2>
      <div className="p-6 mt-8">
        <p>Nombre: {user.firstName} {user.lastName}</p>
        <p>Correo electrónico: {user.email}</p>
        <p>Rol: {user.role}</p>
      </div>
      <UserTable />
    </section>
  );
};

export default AdminProfilePage