import { useState } from 'react'
import './AddCharacteristic.scss'
import { useCharacteristicsContext } from '../../../../../context/CharacteristicsContext';
import apiCharacteristics from '../../../../../application/services/Characteristics';
import ModalSuccess from '../../../molecules/Modals/success/ModalSuccess'
import Button from '../../../atoms/Button/Button'
import CharacteristicTable from '../../../molecules/CharacteristicTable/CharacteristicTable';
const AddCharacteristic = () => {
  const { characteristics, updateCharacteristics } = useCharacteristicsContext();
  const [showModalSuccess, setShowModalSuccess] = useState(false);

  const [characteristicData, setCharacteristicData] = useState({
    characteristicName: '',
    image: '',
  });

  const { characteristicName, image } = characteristicData;

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setCharacteristicData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleImageUpload = (event) => {
    const imageFile = Array.from(event.target.files);
    const updatedImageFiles = [];
    imageFile.forEach(image => {
      const reader = new FileReader();
      reader.onloadend = () => {
        image.url = reader.result;
      };
      reader.readAsDataURL(image);
      updatedImageFiles.push(image);
    });
    setCharacteristicData((prevData) => ({
      ...prevData,
      image: updatedImageFiles[0],
    }));
  };

  const handleAddCharacteristic = async () => {
    const formData = new FormData();
    formData.append('title', characteristicName);
    formData.append('icon', image);

    try {
      const response = await apiCharacteristics.post('/characteristic', formData);
      const updatedCharacteristics = [...characteristics, response.data];
      updateCharacteristics(updatedCharacteristics);
      setShowModalSuccess(true);
    } catch (error) {
      console.log('error al agregar caracteristica', error);
    }
  };


  const handleDeleteCharacteristic = async (id) => {
    try {
      await apiCharacteristics.delete(`/characteristic/${id}`);
      const updatedCharacteristics = characteristics.filter((characteristic) => characteristic.id !== id);
      updateCharacteristics(updatedCharacteristics);
    } catch (error) {
      console.log('error al eliminar caracteristica', error);
    }
  };


  return (
    <>
      <section className='addcharacteristics'>
        <article className="add-product ">
          <ModalSuccess title="Característica agregada" text="La Característica se agregó correctamente" showModalSuccess={showModalSuccess} setShowModalSuccess={setShowModalSuccess} />
          <h2 className='page-title'>Agregar Nueva Característica</h2>
          <form action="" className='add-product__form'>
            <label className='form-label'>Nombre de la Característica</label>
            <input className='form-input' type="text" value={characteristicName} onChange={handleInputChange} name='characteristicName' autoComplete="off" />

            <label className='form-label'>Imagen de la Característica</label>
            <input className='form-input' type="file" onChange={handleImageUpload} name='image' accept=".jpg, .jpeg, .png, .webp .ico .svg" />

          </form>
          <Button type="button" onClick={handleAddCharacteristic} text="Agregar Característica" state="signUp" />
        </article>
        <CharacteristicTable characteristics={characteristics} handleDelete={handleDeleteCharacteristic} />

      </section>
    </>
  )
}

export default AddCharacteristic