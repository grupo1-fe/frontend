import { useState } from 'react';
import ModalSuccess from '../../../molecules/Modals/success/ModalSuccess';
import Button from '../../../atoms/Button/Button';
import apiCategory from '../../../../../application/services/Category';
import { useCategoryContext } from '../../../../../context/CategoryContext';
import './AddCategory.scss';
import CategoryTable from '../../../molecules/CategoryTable/CategoryTable ';
const AddCategory = () => {
  const [showModalSuccess, setShowModalSuccess] = useState(false);
  const [imageFile, setImageFile] = useState(null);

  const { categories, updateCategories } = useCategoryContext();

  const [categoryData, setCategoryData] = useState({
    categoryName: '',
    categoryDescription: '',
  });

  const { categoryName, categoryDescription } = categoryData;

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setCategoryData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleImageUpload = (event) => {


    const imageFile = Array.from(event.target.files);

    const updatedImageFiles = [];


    imageFile.forEach(image => {
      const reader = new FileReader();
      reader.onloadend = () => {
        image.url = reader.result;
      };
      reader.readAsDataURL(image);
      updatedImageFiles.push(image);
    });
    setImageFile(updatedImageFiles);
  };

  const handleAddCategory = async () => {
    const formData = new FormData();
    formData.append('title', categoryName);
    formData.append('description', categoryDescription);
    formData.append('icon', imageFile[0]);

    // console.log('formData', formData);
    try {
      const response = await apiCategory.post('/category', formData);
      // console.log('Respuesta del servidor:', response.data);
      const updatedCategories = [...categories, response.data];
      updateCategories(updatedCategories);
      setShowModalSuccess(true);
      setCategoryData({
        categoryName: '',
        categoryDescription: '',
      });
      setTimeout(() => {
        setShowModalSuccess(false);
      }, 2000);
      resetForm();
    } catch (error) {
      console.error('Error al subir la categoría:', error);
    }
  };

  const resetForm = () => {
    setCategoryData((prevData) => ({
      ...prevData,
      categoryName: '',
      categoryDescription: '',
    }));
    setImageFile([]);
  };


  const handleDeleteCategory = async (categoryId) => {
    try {
      await apiCategory.delete(`/category/${categoryId}`);
      const updatedCategories = categories.filter((category) => category.id !== categoryId);
      updateCategories(updatedCategories);
      setTimeout(() => {
        setShowModalSuccess(false);
      }, 2000);
    } catch (error) {
      console.error('Error al eliminar la categoría:', error);
    }
  };

  return (
    <>
      <section className='addcategory'>
        <article className="add-product ">
          <ModalSuccess title="Categoría agregada" text="La categoría se agregó correctamente" showModalSuccess={showModalSuccess} setShowModalSuccess={setShowModalSuccess} />
          <h2 className='page-title'>Agregar Nueva Categoría</h2>
          <form action="" className='add-product__form'>
            <label className='form-label'>Nombre de la Categoría</label>
            <input className='form-input' type="text" value={categoryName} onChange={handleInputChange} name='categoryName' autoComplete="off" />

            <label className='form-label'>Descripción de la Categoría</label>
            <textarea className='form-input' value={categoryDescription} onChange={handleInputChange} name='categoryDescription' autoComplete="off" />

            <label className='form-label'>Imagen de la Categoría</label>
            <input className='form-input' type="file" onChange={handleImageUpload} name='image' accept=".jpg, .jpeg, .png, .webp .ico .svg" />

          </form>
          <Button type="button" onClick={handleAddCategory} text="Agregar Categoría" state="signUp" />
        </article>

        <CategoryTable categories={categories} handleDelete={handleDeleteCategory} />
      </section>
    </>
  );
};

export default AddCategory;
