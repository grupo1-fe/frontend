import { useState, useEffect } from 'react';
import api from '../../../../adapters/api/axios';
import { useProductContext } from '../../../../context/ProductContext';
import Table from '../../molecules/Table/Table';

import './ProductListPage.scss';
import ModalSuccess from '../../molecules/Modals/success/ModalSuccess';

const ProductListPage = () => {
  const [showList, setShowList] = useState(false);
  const { products, updateProducts } = useProductContext();
  const [showModalSuccess, setShowModalSuccess] = useState(false);

  const handleDelete = async (productId) => {
    try {
      await api.delete(`/products/${productId}`);
      updateProducts(products.filter(product => product.id !== productId));
      setShowModalSuccess(true);
      setTimeout(() => {
        setShowModalSuccess(false);
      }, 2000);
    } catch (error) {
      console.error('Error al eliminar el producto:', error);
    }
  }
  const productList = showList ? products : [];


  return (
    <section className="product-list">
      <button className="button-list" onClick={() => setShowList(!showList)}>
        {showList ? 'Ocultar productos' : 'Ver productos'}
      </button>

      {showList && (
        <>
          {productList.length === 0 ?
            <h2 className="no-product">Opps! todavia no tenes productos</h2>
            :
            <Table product={productList} handleDelete={handleDelete} />
          }
        </>
      )}
      <ModalSuccess title="Producto eliminado" text="El producto se eliminó correctamente" showModalSuccess={showModalSuccess} setShowModalSuccess={setShowModalSuccess} />
    </section>
  );
};

export default ProductListPage;
