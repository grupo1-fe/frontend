import AddProductPage from './AddProductPage';
import ProductListPage from './ProductListPage';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../../../../context/AuthContext';
import './AdminPage.scss';
import AddCategory from './AddCategory/AddCategory';
import AdminProfilePage from './AdminProfilePage/AdminProfilePage';
import AddCharacteristic from './AddCharacteristic/AddCharacteristic';


const AdminPage = () => {
  const { user } = useAuth();
  const navigate = useNavigate();

  if (!user || user.role !== 'admin') {
    return (
      <div className='admin-page h-screen justify-center'>
        <h1 className='font-bold text-xl'>Error 403</h1>
        <button onClick={() => navigate('/')} className='btn btn-primary shadow-sm text-emerald-500'>Volver al inicio</button>
      </div>
    )
  }

  return (
    <section className='admin-page'>
      <AdminProfilePage />
      <AddProductPage />
      <ProductListPage />
      <AddCategory />
      <AddCharacteristic />
    </section>
  );
};

export default AdminPage;
