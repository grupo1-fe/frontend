import  { useState } from 'react';
import { useProductContext } from '../../../../context/ProductContext';
import './HomePage.scss';
import Searcher from '../../organisms/Searcher/Searcher';
import RecommendedCarousel from '../../organisms/RecommendedCarousel/RecommendedCarousel';
import CardHome from '../../molecules/CardHome/CardHome';
import CategoryFilter from '../../molecules/CategoryFilter/CategoryFilter';
import CategoryCard from '../../molecules/CategoryCard/CategoryCard';
import Loader from '../Loader/Loader';

const HomePage = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const productsPerPage = 10;
  const [selectedCategories, setSelectedCategories] = useState([]);
  const { products } = useProductContext();

  const handleCategoryChange = (categoryTitle) => {
    setSelectedCategories(prevCategories => {
      if (prevCategories.includes(categoryTitle)) {
        return prevCategories.filter(cat => cat !== categoryTitle);
      } else {
        return [...prevCategories, categoryTitle];
      }
    });
  };

  const filteredProducts = selectedCategories.length === 0
    ? products
    : products.filter(product => selectedCategories.includes(product.category));

  const totalPages = Math.ceil(filteredProducts.length / productsPerPage);
  const startIndex = (currentPage - 1) * productsPerPage;
  const endIndex = startIndex + productsPerPage;
  const displayedProducts = filteredProducts.slice(startIndex, endIndex);

  if (products.length === 0) {
    return <Loader />;
  } 


  return (
    <>
      <Searcher />
      <CategoryCard />
      <section>
        <CategoryFilter products={products} onCategoryChange={handleCategoryChange} />
      </section>
      <h2 className='text-lg font-bold'>Categorías</h2>
      <div className="product-grid">
        {displayedProducts.map((product) => (
          <CardHome product={product} key={product.id} />
        ))}
      </div>
      <div className="pagination">
        <button
          onClick={() => setCurrentPage(currentPage > 1 ? currentPage - 1 : currentPage)}
          disabled={currentPage === 1}
        >
          Anterior
        </button>
        <span>{currentPage} de {totalPages}</span>
        <button
          onClick={() => setCurrentPage(currentPage < totalPages ? currentPage + 1 : currentPage)}
          disabled={currentPage === totalPages}
        >
          Siguiente
        </button>
      </div>
      <section className="recommended">
        <h2 className='text-lg font-bold'>Recomendaciones</h2>
        <RecommendedCarousel displayedProducts={displayedProducts} />
      </section>


    </>
  );
};

export default HomePage;
