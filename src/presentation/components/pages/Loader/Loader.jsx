import './Loader.scss'

const Loader = () => {
  return (
    <div className="h-screen flex flex-col justify-center">
      <span className="loader"></span>
    </div>
  )
}

export default Loader