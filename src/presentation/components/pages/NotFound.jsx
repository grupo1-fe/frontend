const NotFound = () => {
  return (
    <section className="section-container">
      <div>
        <h1>Oops!</h1>
        <h2>La pagina que buscas no existe o no esta disponible</h2>
      </div>
    </section>
  )
}

export default NotFound