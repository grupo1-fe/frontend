import React, { useState, useEffect } from 'react';
import api from '../../../../adapters/api/axios';
import { useParams, useNavigate } from 'react-router-dom';
import { useCategoryContext } from '../../../../context/CategoryContext';
import { useProductContext } from '../../../../context/ProductContext';
import { useCharacteristicsContext } from '../../../../context/CharacteristicsContext';
import './ProductPage.scss';
import { AiOutlineArrowLeft, AiOutlineShareAlt } from 'react-icons/ai';
import NoImg from '../../../../assets/img/no-img.png';
import Loader from '../Loader/Loader';
const ProductPage = () => {
  const [product, setProduct] = useState(null);
  const [mainImage, setMainImage] = useState('');
  const { productId } = useParams();

  const navigate = useNavigate();

  const { categories } = useCategoryContext();
  const { products } = useProductContext();
  const { characteristics, updatedCharacteristics } = useCharacteristicsContext();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalImage, setModalImage] = useState('');

  const handleImageClick = (imgUrl) => {
    setModalImage(imgUrl);
    setIsModalOpen(true);
  };


  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  useEffect(() => {
    const productFromContext = products.find(p => p.id === productId);
    if (productFromContext) {
      setProduct(productFromContext);
    } else {
      api.get(`/products/${productId}`)
        .then((response) => {
          const productData = response.data;
          setProduct(productData);
          setMainImage(productData.thumbnail);
        })
        .catch((error) => {
          console.error('Error al obtener el producto:', error);
        });
    }
  }, [productId, products]);


  useEffect(() => {
    if (product && updatedCharacteristics.length > 0) {
      const updatedProductCharacteristics = product.characteristic.map((char) => {
        return updatedCharacteristics[char] || char;
      });

      setProduct(prevProduct => ({
        ...prevProduct,
        characteristic: updatedProductCharacteristics,
      }));
    }
  }, [product, updatedCharacteristics]);



  if (!product) {
    return <Loader />;
  }

  const handleGoBack = () => {
    if (window.history.length > 2) {
      navigate(-1);
    } else {
      navigate('/');
    }
  };


  const handleShareClick = async () => {
    try {
      await navigator.share({
        title: product.title,
        text: product.description,
        url: window.location.href,
      });
      console.log('Contenido compartido exitosamente');
    } catch (error) {
      console.error('Error al compartir:', error);
    }
  };


  return (
    <section className="product-page  text-gray-400">

      <div className="product-container">
        <div className='title-container'>

          <button className="btn-back" onClick={handleGoBack}> <AiOutlineArrowLeft /> Volver</button>
        </div>
        <article className="product-hero">
          <figure className="product-figure hover-container">
            {
              product.images.length > 0 ?
                <img src={mainImage.url} alt={product.title} className='product-img' /> :
                <img src={NoImg} alt={product.title} className='no-img' />
            }
            <h3 className="hover-text" onClick={() => handleImageClick(mainImage.url)}>Ver completa</h3>
          </figure>
          <section className="gallery">
            {
              product.images.length > 0 ?
                product.images.map((image, index) => (
                  <>
                    <figure key={index} className={mainImage.url === image.url ? 'gallery-figure-active' : 'gallery-figure hover-container'}>
                      <img
                        src={image.url}
                        alt={`Image ${index + 1}`}
                        className={mainImage.url === image.url ? 'gallery-img-active' : 'gallery-img'}
                        onClick={() => handleImageClick(image.url)} />
                      <h3 className="hover-text-gallery" onClick={() => handleImageClick(image.url)}>Ver completa</h3>
                    </figure>
                  </>
                ))

                :
                < figure className="gallery-figure">
                  <img className="no-img" src={NoImg} alt={product.title} />
                </figure>
            }
          </section>

          {
            isModalOpen && (
              <div className="modal-background" onClick={handleCloseModal}>
                <span className="close-modal-btn" onClick={handleCloseModal}>X</span>
                <img src={modalImage} alt="Zoomed" className="zoomed-image" />
              </div>
            )
          }


        </article>
      </div>
      <section className="product-description-section">
        <h3 className="product-title">
          {product.title}
          <button className="btn-share" onClick={handleShareClick}><AiOutlineShareAlt /></button>
        </h3>
        <hr className="my-3" />
        <span className="product-price">${product.price}</span>

        <h3 className='product-description-title'>Descripción del producto</h3>
        <h3 className='product-description-category'>Categoría: {product.category}</h3>
        <div className='description-container'>
          <p className="product-description-text">{product.description}</p>
        </div>
      </section>
      <section className="characteristics-section">
        {product.characteristic && product.characteristic.length > 0 && (
          <>
            <h3 className="characteristics-title">Características</h3>
            <ul className="characteristics-list">
              {product.characteristic.map((productChar, index) => (
                <li key={index} className="characteristics-list-item flex flex-row gap-4">
                  {characteristics.map((char) =>
                    char.title === productChar ? (
                      <img
                        key={char.id}
                        src={char.icon.url}
                        alt={char.title}
                        className="characteristics-list-item-icon object-cover rounded-full h-10 w-10"
                      />
                    ) : null
                  )}
                  <span className="characteristics-list-item-title">{productChar}</span>
                </li>
              ))}
            </ul>
          </>
        )}
      </section>
    </section >
  );
};

export default ProductPage;
