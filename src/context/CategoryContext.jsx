import { createContext, useContext, useState, useEffect } from 'react';
import apiCategory from '../application/services/Category';


const CategoryContext = createContext();

export const useCategoryContext = () => {
  return useContext(CategoryContext);
};

export const CategoryProvider = ({ children }) => {
  const [categories, setCategories] = useState([]);

  const fetchCategories = async () => {
    try {
      const response = await apiCategory.get('/category');
      setCategories(response.data);
    } catch (error) {
      console.log('error al obtener categorias', error);
    }
  };

  useEffect(() => {
    fetchCategories();
  }, []);

  const updateCategories = (newCategories) => {
    setCategories(newCategories);
  };

  return (
    <CategoryContext.Provider value={{ categories, updateCategories }}>
      {children}
    </CategoryContext.Provider>
  );
};