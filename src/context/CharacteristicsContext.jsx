import { createContext, useContext, useState, useEffect } from 'react';
import apiCharacteristics from '../application/services/Characteristics';

const CharacteristicsContext = createContext();


export const useCharacteristicsContext = () => {
  return useContext(CharacteristicsContext);
}

export const CharacteristicsProvider = ({ children }) => {
  const [characteristics, setCharacteristics] = useState([]);
  const [updatedCharacteristics, setUpdatedCharacteristics] = useState({});

  const fetchCharacteristics = async () => {
    try {
      const response = await apiCharacteristics.get('/characteristic');
      setCharacteristics(response.data);
    } catch (error) {
      console.log('error al obtener caracteristicas', error);
    }
  };

  useEffect(() => {
    fetchCharacteristics();
  }, []);

  const updateCharacteristics = (newCharacteristics) => {
    setCharacteristics(newCharacteristics);
  };

  const updateUpdatedCharacteristics = (charId, title) => {
    setUpdatedCharacteristics(prev => ({ ...prev, [charId]: title }));
  };

  return (
    <CharacteristicsContext.Provider value={{ characteristics, updateCharacteristics, updatedCharacteristics, updateUpdatedCharacteristics }}>
      {children}
    </CharacteristicsContext.Provider>
  );
};
