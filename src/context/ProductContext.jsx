import { createContext, useContext, useState, useEffect } from 'react';
import api from '../adapters/api/axios';

const ProductContext = createContext();

export const useProductContext = () => {

  return useContext(ProductContext);
};

export const ProductProvider = ({ children }) => {
  const [products, setProducts] = useState([]);


  const fetchProducts = async () => {
    try {
      const response = await api.get('/products');
      setProducts(response.data);
    } catch (error) {
      console.log('error al obtener productos', error);
    }
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  const updateProducts = (newProducts) => {
    setProducts(newProducts);
  };

  return (
    <ProductContext.Provider value={{ products, updateProducts }}>
      {children}
    </ProductContext.Provider>
  );
};
