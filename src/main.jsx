import React from 'react'
import ReactDOM from 'react-dom/client'
import { ProductProvider } from './context/ProductContext.jsx'
import { AuthProvider } from './context/AuthContext.jsx'
import { CategoryProvider } from './context/CategoryContext.jsx'
import { BrowserRouter } from 'react-router-dom'
import App from './App.jsx'
import './index.scss'
import { CharacteristicsProvider } from './context/CharacteristicsContext.jsx'

ReactDOM.createRoot(document.getElementById('root')).render(

  <AuthProvider>
    <CategoryProvider>
      <CharacteristicsProvider>
        <BrowserRouter>
          <ProductProvider>
            <App />
          </ProductProvider>
        </BrowserRouter>
      </CharacteristicsProvider>
    </CategoryProvider>
  </AuthProvider>

)