import { Route, Routes } from 'react-router-dom';
import routes from './config/routes';
import './presentation/styles/App.scss';
import Header from './presentation/components/organisms/Header/Header';
import MainLayaout from './presentation/components/templates/MainLayout';
import Footer from './presentation/components/organisms/Footer/Footer';
function App() {
  return (
    <>
      <Header />
      <MainLayaout>
        <Routes>
          {routes.map((route, index) => (
            <Route key={index} path={route.path}
              element={<route.component />}
            />
          ))}
        </Routes >
      </MainLayaout>
      <Footer />
    </>
  )
}

export default App 