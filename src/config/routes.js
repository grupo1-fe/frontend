import HomePage from "../presentation/components/pages/HomePage/HomePage"
import ContactSection from "../presentation/components/organisms/ContactSection/ContactSection"
import NotFound from "../presentation/components/pages/NotFound"
import Nosotros from "../presentation/components/organisms/Nosotros"
import ProductPage from "../presentation/components/pages/ProductPage/ProductPage"
import AdminPage from "../presentation/components/pages/admin/AdminPage";
import AdminProfilePage from '../presentation/components/pages/admin/AdminProfilePage/AdminProfilePage';
import AdminEditProductPage from '../presentation/components/pages/admin/AdminEditProductPage/AdminEditProductPage';
import UserProfilePage from '../presentation/components/pages/UserProfilePage/UserProfilePage';
import Login from "../presentation/components/pages/Login/Login";
import Register from "../presentation/components/pages/Register/Register";
import AddCategory from "../presentation/components/pages/admin/AddCategory/AddCategory"

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomePage
  },
  {
    path: '/contacto',
    name: 'Contacto',
    component: ContactSection,
    exact: true
  },
  {
    path: '/nosotros',
    name: 'Nosotros',
    component: Nosotros,
    exact: true
  },
  {
    path: '/admin/inicio',
    name: 'AdminPage',
    component: AdminPage,
    exact: true,
    adminRoute: true
  },
  {
    path: '/producto/:productId',
    name: 'Producto',
    component: ProductPage,
    exact: true
  },
  {
    path: '/perfil',
    name: 'UserProfilePage',
    component: UserProfilePage,
    exact: true,
    roles: ['user', 'admin'],
  },
  {
    path: '/perfil-admin',
    name: 'AdminProfilePage',
    component: AdminProfilePage,
    exact: true,
    roles: ['admin'],
  },
  {
    path: '/admin/agregar-categoria',
    name: 'AddCategory',
    component: AddCategory,
    exact: true,
  },
  {
    path: '/admin/editar-producto/:productId',
    name: 'AdminEditProductPage',
    component: AdminEditProductPage,
    exact: true,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    exact: true,
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    exact: true,
  },
  {
    path: "*",
    component: NotFound
  }
]
export default routes