# Equipo 01

## Para levantar el proyecto 

cambiar el nombre del archivo .env renombrar a .env

1- Instalar dependencias:
```bash
  npm install
```
2- Levantar proyecto:
```bash
  npm run dev
```


# Documentacion de la API
## 📦 Endpoints
---
## API URL:
```bash
  http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api
```
---
#### Product

Base URL: /api/product

Full URL: `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/product`

##### - Crear producto

  - Endpoint: /admin/guardar
  - Method: POST
  - Body: Content-type: multipart/form-data

 #####  - Full url POST `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/product/admin/guardar`

```json
{
  "title": "Product Title",
  "description": "Product Description",
  "thumbnail": "IMG",
  "images": ["IMG1", "IMG2", "IMG3", "IMG4"],
  "httpCategory": {
    "id": "CATEGORY_ID",
    "category": "Category Name",
    "description": "Category Description",
    "thumbnail": "IMG"
  },
  "characteristics": [
    {
      "id": "CHARACTERISTIC_ID",
      "name": "Characteristic Name",
      "icon": "IMG"
    },
  ]
}
```

##### - Traer todos los productos

  - Endpoint: /public/traerTodo
  - Method: GET

#####  - Full url GET `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/product/public/traerTodo`

##### - Traer un producto

 - Endpoint: /public/detalle/ID
 - Method: GET

#####  - Full url GET `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/product/public/detalle/ID`

#### - Modificar producto

  - Endpoint: /admin/modificar
  - Method: PUT
  - Body: Content-type: multipart/form-data

##### - Full url PUT `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/product/admin/modificar`

```json
{
  "id": "PRODUCT_ID",
  "title": "Product Title",
  "description": "Product Description",
  "thumbnail": "IMG",
  "images": ["IMG1", "IMG2", "IMG3", "IMG4"],
  "httpCategory": {
    "id": "CATEGORY_ID"
  },
  "httpCharacteristics": [
    {
      "id": "CHARACTERISTIC_ID"
    },
    ...
  ]
}
```

#### - Modificar algo del producto

  - Endpoint: /admin/parcialMod
  - Method: PATCH
  - Body: Content-type: multipart/form-data

##### - Full url PUT `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/product/admin/parcialMod`

```json
{
  "id": "PRODUCT_ID",
  ...
}
```

#### - Eliminar producto

  - Endpoint: /admin/eliminar/ID
  - Method: DELETE

  ##### - Full url DELETE `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/product/admin/eliminar/ID`


#### Category

Base URL: /api/category

##### - Crear categoria

  - Endpoint: /admin/guardar
  - Method: POST
  - Body: Content-type: multipart/form-data

##### - Full url POST `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/category/admin/guardar`

```json
{
    "category": "Category Name",
    "description": "Category Description",
    "thumbnail": "IMG"
}
```

##### - Traer todas las categorias

  - Endpoint: /public/traerTodo
  - Method: GET

##### - Full url GET `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/category/public/traerTodo`

#### - Modificar categoria
  
    - Endpoint: /admin/modificar
    - Method: PUT
    - Body: Content-type: multipart/form-data

##### - Full url PUT `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/category/admin/modificar`

```json
{
    "id": "CATEGORY_ID",
    "category": "Category Name",
    "description": "Category Description",
    "thumbnail": "IMG"
}
```

#### - Modificar algo de la categoria

  - Endpoint: /admin/parcialMod
  - Method: PATCH
  - Body: Content-type: multipart/form-data

##### - Full url PUT `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/category/admin/parcialMod`

```json
{
  "id": "CATEGORY_ID",
  ...
}
```

#### - Filtrar productos por categoria
  
    - Endpoint: `filtrarProdCat?categorias=CATEGORY1&categorias=CATEGORY2`
    - Method: GET

##### - Full url GET `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/category/public/filtrarProdCat?categorias=CATEGORY1&categorias=CATEGORY2`

#### - Eliminar categoria
  
    - Endpoint: /admin/eliminar/ID
    - Method: DELETE

##### - Full url DELETE `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/category/admin/eliminar/ID`

#### Characteristic

Base URL: `/api/character`

#### - Guardar caracteristica

  - Endpoint: /admin/guardar
  - Method: POST
  - Body: Content-type: multipart/form-data

##### - Full url POST `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/character/admin/guardar`

```json
{
    "name": "Characteristic Name",
    "icon": "IMG"
}
```

#### - Traer todas las caracteristicas

  - Endpoint: /public/traerTodo
  - Method: GET

##### - Full url GET `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/character/public/traerTodo`


#### - Modificar caracteristica

    - Endpoint: /admin/modificar
    - Method: PUT
      Body: Content-type: multipart/form-data

  ##### - Full url PUT `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/character/admin/modificar`

```json
{
    "id": "CHARACTERISTIC_ID",
    "name": "Characteristic Name",
    "icon": "IMG"
}
```

#### - Modificar algo de la caracteristica

  - Endpoint: /admin/parcialMod
  - Method: PATCH
  - Body: Content-type: multipart/form-data

##### - Full url PUT `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/character/admin/parcialMod`

```json
{
  "id": "CHARACTERISTIC_ID",
  ...
}
```

#### - Eliminar caracteristica
    
      - Endpoint: /admin/eliminar/ID
      - Method: DELETE

##### - Full url DELETE `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/character/admin/eliminar/ID`

#### User

Base URL: /api/user

##### - Crear usuario

  - Endpoint: /admin/guardar
  - Method: POST
  - Body: Content-type: application/json

##### - Full url POST `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/user/public/guardar`

```json
{
    "nombre": "nombre",
    "apellido": "apellido",
    "email": "mail@mail.com",
    "password": "password"
}

```

##### - Traer todos los usuarios

  - Endpoint: /admin/traerTodo
  - Method: GET

##### - Full url GET `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/user/admin/traerTodo`

##### - Traer todos los roles

  - Endpoint: /admin/roles
  - Method: GET
  
##### - Full url GET `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/user/admin/roles`

##### - Traer un usuario

  - Endpoint: /admin/detalle/ID
  - Method: GET
  
##### - Full url GET `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/user/admin/detalle/ID`

#### - Modificar usuario
  
    - Endpoint: /admin/modificar
    - Method: PUT
    - Body: Content-type: application/json

##### - Full url PUT `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/user/admin/modificar`

```json
{
    "id": "id",
    "nombre": "nombre",
    "apellido": "apellido",
    "password": "password",
    "rol":{
      "id": "id",
      "rol": "ROL"
    }
}

```

#### - Modificar algo del usuario
  
    - Endpoint: /admin/parcialMod
    - Method: PATCH
    - body: Content-type: application/json

##### - Full url PUT `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/user/admin/parcialMod`

```json
{
    "id": "id",
    ...
}

```

#### - Eliminar usuario
  
    - Endpoint: /admin/eliminar/ID
    - Method: DELETE

##### - Full url DELETE `http://ec2-18-117-192-179.us-east-2.compute.amazonaws.com:8080/api/user/admin/eliminar/ID`



```js
{
    "id": 41,
    "title": "jaja",
    "description": "ggvjh",
    "thumbnail": "https://c4-grupo1.s3.amazonaws.com/jaja/portada",
    "images": [
        "https://c4-grupo1.s3.amazonaws.com/jaja/galeria0",
        "https://c4-grupo1.s3.amazonaws.com/jaja/galeria1",
        "https://c4-grupo1.s3.amazonaws.com/jaja/galeria2",
        "https://c4-grupo1.s3.amazonaws.com/jaja/galeria3"
    ],
    "categoryDTO": null,
    "characteristics": null
}

```

--- 
## 📨 Metodo post exitoso:

```javscript
import axios from 'axios';

const api = axios.create({
  baseURL: 'http://18.117.192.179:8080/api/product', 
  timeout: 5000, 

  headers: {
    'Content-Type': 'multipart/form-data',
  }, 
});

export default api;

```




## Envio
```js
import axios from 'axios';

const api = axios.create({
  baseURL: 'http://18.117.192.179:8080/api/product', 
  timeout: 5000, 

  headers: {
    'Content-Type': 'multipart/form-data',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*',
  }, 
});

export default api;




```
## Respuesta

```javascript
categoryDTO: null
characteristics: null
description: "tessttessttesst"
id: 55
images: Array(3)
0: "https://c4-grupo1.s3.amazonaws.com/tesst/galeria0"
1: "https://c4-grupo1.s3.amazonaws.com/tesst/galeria1"
2: "https://c4-grupo1.s3.amazonaws.com/tesst/galeria2"

thumbnail: "https://c4-grupo1.s3.amazonaws.com/tesst/portada"
title :  "tesst"
```
